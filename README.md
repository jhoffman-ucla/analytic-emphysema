# Analytic Emphysema

## Annotation instructions

The goal of our annotations is to place an ROI/segmentation in the trachea several millimeters superior to the carina to obtain a reasonable measurement of noise in air for the given scan.  We also want to place one in the aortic arch for noise comparision.  These instructions should guide you on how to annotate a single scan.

We'll use [ITK-SNAP](https://itksnap.org).

### Step 1: Open the study

Start ITk-SNAP and click the "Open Image..." in the bottom right.

Under the "File Format" dropdown, select "Dicom Image Series"

Click "Browse..." and navigate to the desired study.  Select an image from the series (counterintuitively, even though we're opening the whole study you only select a single image).  Click "Open".

Click "Next >".  You should only see one series in the list, so click "Next >" again to open the series.  Finally click "Finish."

![](images/Step1-OpenTheStudy.gif)

### Step 2: Locate the carina in the axial view

The [carina](https://en.wikipedia.org/wiki/Carina_of_trachea) is a ridge of cartilege where the trachea branches into two brochi.  To identify it (or the nearest slice to it), start at the superior end of the subject scan (the "head" end).  There will be small circle of air in the middle of the patient; this is the trachea.  Scroll through the slices towards the inferior end of the scan (the "feet" end) visually following the trachea.  As the you get into the lungs and heart, the trachea will seemingly split into two smaller air regions; these are the bronchi.  The slice at which this branch occurs (or we get closest to the actual branch point) will be the slice we identify as "carina."

![](images/Step2-LocateCarina.gif)

### Step 3: ROI Placement

#### Trachea placement

ROI placement in ITK-SNAP can be done a few different ways.  We'll be using the "polygon" tool.

Additionally, since we'll be placing two rois (one for the trachea, and one for the aorta), it's important to make sure that we're selecting the correct layer.  We'll use Layer 1 (default color: red) for the trachea, and Layer 2 (default color: green) for the aorta.

In the "Main Toolbar," click on the "Polygon Mode" button (dots connected with lines).  Then, click on the "Quick Label Picker" (the small colored box, second icon from the last in the main toolbar), and verify that Label 1 (red) is selected.

Zoom in on the trachea (right-click in axial view + move mouse forward), and place the ROI by clicking a series of points completely inside the trachea.  Note: it's important to stay away from the edge of the trachea as this could influence our measurements in an undesirable way.  See the GIF below for more guidance on correct anatomical placement.

Finally, click the "accept" button below the axial view; you should see the polygon "fill-in" with our label color.

![](images/Step3-TracheaPlacement.gif)

#### Aortic arch placement

The aortic arch ROI in most cases will be adjacent to the carina (perhaps even in the same slice).  This is a slightly less important ROI for us, but will be valuable to have for noise comparison. Use the GIF and instructions from John (or another trained reader) for where to place this ROI.

Instructions for placement are the same as for the trachea, however we need to make sure that "Label 2" is selected instead of "Label 1".

![](images/Step3-AorticArchPlacement.gif)

### Step 4: Export the markings back into the study directory

In the menu-bar, select "Segmentation -> Save segmentation image..."

Set the "Image filename" to be "trachea-aorta-rois.nii.gz." Verify that the File Format is "NiFTI" and the "Path" is the same directory that the DICOM images came from.

Click "Finish."

![](images/Step4-ExportMarkings.gif)

### Step 5: Close the case

In the menu bar, "File -> Close all images". 

Repeat steps 1-5 for the remaining cases.